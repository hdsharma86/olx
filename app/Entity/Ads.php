<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    protected $fillable = ['title', 'body'];
}
