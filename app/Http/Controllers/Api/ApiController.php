<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\Ads as Ads;

class ApiController extends Controller
{
    public function index()
    {
        return Ads::all();
    }
}
